{
  "spriteId": {
    "name": "spr_Boundary_01",
    "path": "sprites/spr_Boundary_01/spr_Boundary_01.yy",
  },
  "solid": true,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": null,
  "physicsObject": true,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.0,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": true,
  "physicsShapePoints": [
    {"x":0.0,"y":0.0,},
    {"x":87.0,"y":0.0,},
    {"x":87.0,"y":69.0,},
    {"x":0.0,"y":69.0,},
  ],
  "eventList": [
    {"isDnD":true,"eventNum":0,"eventType":4,"collisionObjectId":{"name":"obj_Player_Submarine_01","path":"objects/obj_Player_Submarine_01/obj_Player_Submarine_01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Objects",
    "path": "folders/Objects.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_Boundary_Parent",
  "tags": [
    "Parent",
    "Boundary",
  ],
  "resourceType": "GMObject",
}