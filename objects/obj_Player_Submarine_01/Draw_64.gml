/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 6DF32A86
/// @DnDArgument : "x" "10"
/// @DnDArgument : "y" "10"
/// @DnDArgument : "caption" ""Health =  ""
/// @DnDArgument : "var" "Hull_Strength"
draw_text(10, 10, string("Health =  ") + string(Hull_Strength));

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 279C72F6
/// @DnDArgument : "x" "10"
/// @DnDArgument : "y" "40"
/// @DnDArgument : "caption" ""Fishes Colleceted = ""
/// @DnDArgument : "var" "Fishes_Collected"
draw_text(10, 40, string("Fishes Colleceted = ") + string(Fishes_Collected));