{
  "spriteId": {
    "name": "spr_Fish_03",
    "path": "sprites/spr_Fish_03/spr_Fish_03.yy",
  },
  "solid": true,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": null,
  "physicsObject": true,
  "physicsSensor": false,
  "physicsShape": 0,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [
    {"x":16.0,"y":16.0,},
    {"x":11.0,"y":16.0,},
  ],
  "eventList": [
    {"isDnD":true,"eventNum":0,"eventType":4,"collisionObjectId":{"name":"obj_Player_Submarine_01","path":"objects/obj_Player_Submarine_01/obj_Player_Submarine_01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "Objects",
    "path": "folders/Objects.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_Fish_03",
  "tags": [
    "Fish",
  ],
  "resourceType": "GMObject",
}