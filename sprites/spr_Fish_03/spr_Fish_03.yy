{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 35,
  "bbox_top": 4,
  "bbox_bottom": 16,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 39,
  "height": 20,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ab7964c0-2208-4c11-bf25-74b4cc0bd9f2","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ab7964c0-2208-4c11-bf25-74b4cc0bd9f2","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"LayerId":{"name":"18ee8090-fed0-4b7f-94cf-cd1b0c30a2ea","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Fish_03","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","name":"ab7964c0-2208-4c11-bf25-74b4cc0bd9f2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e58567e2-2eaa-4ac7-9a52-28a79fb74952","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e58567e2-2eaa-4ac7-9a52-28a79fb74952","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"LayerId":{"name":"18ee8090-fed0-4b7f-94cf-cd1b0c30a2ea","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Fish_03","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","name":"e58567e2-2eaa-4ac7-9a52-28a79fb74952","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6edb0825-a3a5-44fa-829e-9ac42831693f","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6edb0825-a3a5-44fa-829e-9ac42831693f","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"LayerId":{"name":"18ee8090-fed0-4b7f-94cf-cd1b0c30a2ea","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Fish_03","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","name":"6edb0825-a3a5-44fa-829e-9ac42831693f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"03a4ac09-b14a-486c-b592-68ab8192e75a","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"03a4ac09-b14a-486c-b592-68ab8192e75a","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"LayerId":{"name":"18ee8090-fed0-4b7f-94cf-cd1b0c30a2ea","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Fish_03","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","name":"03a4ac09-b14a-486c-b592-68ab8192e75a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Fish_03","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2af8ee88-d667-4136-a3db-1cc7cb3a46ac","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab7964c0-2208-4c11-bf25-74b4cc0bd9f2","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f461a52f-794c-4dec-aa6e-1fa41ac2bf37","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e58567e2-2eaa-4ac7-9a52-28a79fb74952","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6079ea39-12ac-461f-97f7-7705f859a11e","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6edb0825-a3a5-44fa-829e-9ac42831693f","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e42b9650-eaeb-4cd3-a6ba-4f54ffaa320d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"03a4ac09-b14a-486c-b592-68ab8192e75a","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Fish_03","path":"sprites/spr_Fish_03/spr_Fish_03.yy",},
    "resourceVersion": "1.4",
    "name": "spr_Fish_03",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"18ee8090-fed0-4b7f-94cf-cd1b0c30a2ea","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Fish_03",
  "tags": [
    "Fish",
  ],
  "resourceType": "GMSprite",
}