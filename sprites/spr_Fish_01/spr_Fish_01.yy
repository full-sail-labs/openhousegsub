{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 27,
  "bbox_top": 6,
  "bbox_bottom": 25,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"7426105d-2e9c-48c6-bc26-92c1521a55c1","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7426105d-2e9c-48c6-bc26-92c1521a55c1","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"LayerId":{"name":"d1524f1e-ffeb-4a1c-ade0-dc4c2b1e6bb0","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Fish_01","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","name":"7426105d-2e9c-48c6-bc26-92c1521a55c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2ceb66be-b8ef-48a8-a0d6-aae11e9ec0a8","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2ceb66be-b8ef-48a8-a0d6-aae11e9ec0a8","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"LayerId":{"name":"d1524f1e-ffeb-4a1c-ade0-dc4c2b1e6bb0","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Fish_01","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","name":"2ceb66be-b8ef-48a8-a0d6-aae11e9ec0a8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"30c68106-e217-45b5-89ca-64b0b790ae7c","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"30c68106-e217-45b5-89ca-64b0b790ae7c","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"LayerId":{"name":"d1524f1e-ffeb-4a1c-ade0-dc4c2b1e6bb0","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Fish_01","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","name":"30c68106-e217-45b5-89ca-64b0b790ae7c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1052ae6f-f0ef-4a69-97d0-d352b11ac9b6","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1052ae6f-f0ef-4a69-97d0-d352b11ac9b6","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"LayerId":{"name":"d1524f1e-ffeb-4a1c-ade0-dc4c2b1e6bb0","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Fish_01","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","name":"1052ae6f-f0ef-4a69-97d0-d352b11ac9b6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Fish_01","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f275c0e1-630b-49b1-b87c-05b998a58918","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7426105d-2e9c-48c6-bc26-92c1521a55c1","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4220df06-565c-41ea-b422-426651e286f4","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ceb66be-b8ef-48a8-a0d6-aae11e9ec0a8","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8fe91a79-2463-43da-a98d-8e20e9d1019f","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"30c68106-e217-45b5-89ca-64b0b790ae7c","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a2f12c1e-3700-4663-be5f-f463851dc0fc","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1052ae6f-f0ef-4a69-97d0-d352b11ac9b6","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Fish_01","path":"sprites/spr_Fish_01/spr_Fish_01.yy",},
    "resourceVersion": "1.4",
    "name": "spr_Fish_01",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d1524f1e-ffeb-4a1c-ade0-dc4c2b1e6bb0","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Fish_01",
  "tags": [
    "Fish",
  ],
  "resourceType": "GMSprite",
}