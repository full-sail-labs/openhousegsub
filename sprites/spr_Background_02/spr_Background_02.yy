{
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "sequence": {
    "length": 1.0,
    "playback": 1,
    "spriteId": {"name":"spr_Background_02","path":"sprites/spr_Background_02/spr_Background_02.yy",},
    "playbackSpeed": 30.0,
    "tracks": [
      {"name":"frames","keyframes":{"Keyframes":[
            {"id":"f42aa327-cbb3-4a5f-b256-e4b28063aa2b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ec22de7c-bcb0-4a72-adc5-f1dd60e09cb6","path":"sprites/spr_Background_02/spr_Background_02.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"spriteId":null,"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "timeUnits": 1,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Background_02","path":"sprites/spr_Background_02/spr_Background_02.yy",},
    "resourceVersion": "1.4",
    "name": "spr_Background_02",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 959,
  "bbox_top": 0,
  "bbox_bottom": 511,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 960,
  "height": 512,
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ec22de7c-bcb0-4a72-adc5-f1dd60e09cb6","path":"sprites/spr_Background_02/spr_Background_02.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ec22de7c-bcb0-4a72-adc5-f1dd60e09cb6","path":"sprites/spr_Background_02/spr_Background_02.yy",},"LayerId":{"name":"6938178d-53a6-40a5-87e8-6695c6e2f480","path":"sprites/spr_Background_02/spr_Background_02.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Background_02","path":"sprites/spr_Background_02/spr_Background_02.yy",},"resourceVersion":"1.0","name":"ec22de7c-bcb0-4a72-adc5-f1dd60e09cb6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6938178d-53a6-40a5-87e8-6695c6e2f480","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Background_02",
  "tags": [
    "BG",
  ],
  "resourceType": "GMSprite",
}