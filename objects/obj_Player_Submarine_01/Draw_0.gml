/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 3BB9594B
/// @DnDArgument : "var" "phy_speed_x"
/// @DnDArgument : "op" "1"
if(phy_speed_x < 0)
{
	/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
	/// @DnDVersion : 1
	/// @DnDHash : 48C939B8
	/// @DnDParent : 3BB9594B
	/// @DnDArgument : "x" "x"
	/// @DnDArgument : "y" "y"
	/// @DnDArgument : "xscale" "-0.15"
	/// @DnDArgument : "yscale" "0.15"
	/// @DnDArgument : "sprite" "spr_Submarine"
	/// @DnDSaveInfo : "sprite" "spr_Submarine"
	draw_sprite_ext(spr_Submarine, 0, x, y, -0.15, 0.15, 0, $FFFFFF & $ffffff, 1);
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 08827791
else
{
	/// @DnDAction : YoYo Games.Drawing.Draw_Sprite_Transformed
	/// @DnDVersion : 1
	/// @DnDHash : 30208D3A
	/// @DnDParent : 08827791
	/// @DnDArgument : "x" "x"
	/// @DnDArgument : "y" "y"
	/// @DnDArgument : "xscale" "0.15"
	/// @DnDArgument : "yscale" "0.15"
	/// @DnDArgument : "sprite" "spr_Submarine"
	/// @DnDSaveInfo : "sprite" "spr_Submarine"
	draw_sprite_ext(spr_Submarine, 0, x, y, 0.15, 0.15, 0, $FFFFFF & $ffffff, 1);
}