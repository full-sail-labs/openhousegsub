/// @DnDAction : YoYo Games.Common.Variable
/// @DnDVersion : 1
/// @DnDHash : 2C2A1CAA
/// @DnDArgument : "expr" "-1"
/// @DnDArgument : "expr_relative" "1"
/// @DnDArgument : "var" "obj_Player_Submarine_01.Hull_Strength"
obj_Player_Submarine_01.Hull_Strength += -1;

/// @DnDAction : YoYo Games.Miscellaneous.Debug_Show_Message
/// @DnDVersion : 1
/// @DnDHash : 1C622304
/// @DnDInput : 2
/// @DnDArgument : "msg" ""Current Hull Strength = ""
/// @DnDArgument : "msg_1" "obj_Player_Submarine_01.Hull_Strength"
show_debug_message(string("Current Hull Strength = ") + @"
" + string(obj_Player_Submarine_01.Hull_Strength));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6C088CFE
/// @DnDArgument : "var" "obj_Player_Submarine_01.Hull_Strength"
/// @DnDArgument : "op" "3"
if(obj_Player_Submarine_01.Hull_Strength <= 0)
{
	/// @DnDAction : YoYo Games.Rooms.Restart_Room
	/// @DnDVersion : 1
	/// @DnDHash : 23875577
	/// @DnDParent : 6C088CFE
	room_restart();
}