/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 314E3441
/// @DnDArgument : "var" "Thrust_Left"
/// @DnDArgument : "value" "true"
if(Thrust_Left == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3670F4B3
	/// @DnDParent : 314E3441
	/// @DnDArgument : "expr" "-Thrust_Increment"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "phy_speed_x"
	phy_speed_x += -Thrust_Increment;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 628EB79B
/// @DnDArgument : "var" "Thrust_Right"
/// @DnDArgument : "value" "true"
if(Thrust_Right == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 754169BF
	/// @DnDParent : 628EB79B
	/// @DnDArgument : "expr" "Thrust_Increment"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "phy_speed_x"
	phy_speed_x += Thrust_Increment;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 50021210
/// @DnDArgument : "var" "Thrust_Up"
/// @DnDArgument : "value" "true"
if(Thrust_Up == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 6691F5F7
	/// @DnDParent : 50021210
	/// @DnDArgument : "expr" "-Thrust_Increment"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "phy_speed_y"
	phy_speed_y += -Thrust_Increment;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 04A6AFCA
/// @DnDArgument : "var" "Thrust_Down"
/// @DnDArgument : "value" "true"
if(Thrust_Down == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 45F8ACE0
	/// @DnDParent : 04A6AFCA
	/// @DnDArgument : "expr" "Thrust_Increment"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "phy_speed_y"
	phy_speed_y += Thrust_Increment;
}

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 6EA098BF
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_x"
/// @DnDArgument : "arg" ""Background_01""
/// @DnDArgument : "arg_1" "obj_Player_Submarine_01.x * .2"
layer_x("Background_01", obj_Player_Submarine_01.x * .2);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 4C1E7C51
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_y"
/// @DnDArgument : "arg" ""Background_01""
/// @DnDArgument : "arg_1" "obj_Player_Submarine_01.y * .2"
layer_y("Background_01", obj_Player_Submarine_01.y * .2);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 6F372AD8
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_x"
/// @DnDArgument : "arg" ""Background_02""
/// @DnDArgument : "arg_1" "obj_Player_Submarine_01.x * 0.3"
layer_x("Background_02", obj_Player_Submarine_01.x * 0.3);

/// @DnDAction : YoYo Games.Common.Function_Call
/// @DnDVersion : 1
/// @DnDHash : 16841E69
/// @DnDInput : 2
/// @DnDArgument : "function" "layer_y"
/// @DnDArgument : "arg" ""Background_02""
/// @DnDArgument : "arg_1" "obj_Player_Submarine_01.y * 0.3"
layer_y("Background_02", obj_Player_Submarine_01.y * 0.3);

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 67255D2A
/// @DnDArgument : "var" "Fishes_Collected"
/// @DnDArgument : "value" "Total_Fishes"
if(Fishes_Collected == Total_Fishes)
{
	/// @DnDAction : YoYo Games.Game.Restart_Game
	/// @DnDVersion : 1
	/// @DnDHash : 04829D4B
	/// @DnDParent : 67255D2A
	game_restart();
}