/// @DnDAction : YoYo Games.Miscellaneous.Debug_Show_Message
/// @DnDVersion : 1
/// @DnDHash : 75AE9145
/// @DnDArgument : "msg" ""Explosion Detected""
show_debug_message(string("Explosion Detected"));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 32639F9E
/// @DnDArgument : "var" "CanTrigger"
/// @DnDArgument : "value" "true"
if(CanTrigger == true)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 587C931F
	/// @DnDParent : 32639F9E
	/// @DnDArgument : "var" "obj_Player_Submarine_01.Hull_Strength"
	obj_Player_Submarine_01.Hull_Strength = 0;

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 78C87BC2
	/// @DnDParent : 32639F9E
	/// @DnDArgument : "xpos" "x"
	/// @DnDArgument : "ypos" "y"
	/// @DnDArgument : "objectid" "obj_FX_ExplosionBig"
	/// @DnDArgument : "layer" ""Hazards""
	/// @DnDSaveInfo : "objectid" "obj_FX_ExplosionBig"
	instance_create_layer(x, y, "Hazards", obj_FX_ExplosionBig);

	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 65BA121A
	/// @DnDParent : 32639F9E
	/// @DnDArgument : "expr" "false"
	/// @DnDArgument : "var" "CanTrigger"
	CanTrigger = false;

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 7468C6D4
	/// @DnDParent : 32639F9E
	/// @DnDArgument : "steps" "60"
	alarm_set(0, 60);

	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 427C8FBD
	/// @DnDParent : 32639F9E
	/// @DnDArgument : "alpha" "0"
	draw_set_alpha(0);
}